#include <assert.h>
#include <inttypes.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <turbojpeg.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
  if (argc < 2)
    return 1;
  int fd = open(argv[1], O_RDONLY);
  if (fd < 0)
    return 2;
  struct stat s;
  int ret = fstat(fd, &s);
  assert(!ret);
  uint8_t *data = mmap(0, s.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
  assert(data != MAP_FAILED);


  tjhandle jpeg = tjInitDecompress();
  int width, height, jpegsubsamp;

  struct timeval tv1, tv2;
  gettimeofday(&tv1, NULL);

  tjDecompressHeader2(jpeg, data, s.st_size, &width, &height, &jpegsubsamp);
  assert(jpegsubsamp == 2);
  uint8_t* yuv_data = malloc(TJBUFSIZEYUV(width, height, jpegsubsamp));
  tjDecompressToYUV(jpeg, data, s.st_size, yuv_data, 0);

  gettimeofday(&tv2, NULL);  
  printf("Turbo stuff = %f ms\n",
         (double)(tv2.tv_usec - tv1.tv_usec) / 1000 +
             (double)(tv2.tv_sec - tv1.tv_sec));

  int stride = width;
  int size = stride * height;
  {
    int ofd = open("/tmp/asd.bmp", O_WRONLY | O_CREAT | O_TRUNC,
		  S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
    static uint8_t header[54] = {66, 77, 0, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0, 40,
				 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0,  1, 0, 24};
    uint32_t *p = (uint32_t *)&header[2];
    *p = 54 + size;
    p = (uint32_t *)&header[18];
    *p = width;
    p = (uint32_t *)&header[22];
    *p = height;

    printf("w %d\n", width);
    write(ofd, header, 54);
    uint8_t* row = malloc(width * 3);
    for (int y = height - 1; y >= 0; --y) {
      int x = 0;
      for (; x < width; ++x) {
	memset(&row[x*3], yuv_data[y * width  + x], 3);
      }
      write(ofd, row, width);
    }
    free(row);
    close(ofd);
  }
  free(yuv_data);
  munmap(data, s.st_size);
  close(fd);
  return 0;
}
