#include "parse.h"

#include <assert.h>
#include <fcntl.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

void InverseDCT8x8(int16_t *coeffs, uint8_t num, uint8_t *dequant, int stride,
                   uint8_t *output) {
  double c0 = 1 / sqrt(8.0);
  double cr = 0.5;

  for (int m = 0; m < 8; ++m) {
    for (int n = 0; n < 8; ++n) {
      int mz = GetZigzag(m * 8 + n) / 8;
      int nz = GetZigzag(m * 8 + n) % 8;
      double v = 0;
      uint8_t coeffs_left = num;
      for (int p = 0; coeffs_left && p < 8; ++p) {
        for (int q = 0; coeffs_left && q < 8; ++q, --coeffs_left) {

          int pz = GetZigzag(p * 8 + q) / 8;
          int qz = GetZigzag(p * 8 + q) % 8;

          v += (pz ? cr : c0) * (qz ? cr : c0) * coeffs[p * 8 + q] *
               dequant[p * 8 + q] * cos((M_PI * (2 * mz + 1) * pz) / 16) *
               cos((M_PI * (2 * nz + 1) * qz) / 16);
        }
      }
      v += 128;
      v = v < 0 ? 0 : v;
      v = v > 255 ? 255 : v;
      output[mz * stride + nz * 3 + 0] = v;
      output[mz * stride + nz * 3 + 1] = v;
      output[mz * stride + nz * 3 + 2] = v;
    }
  }
}

void ConvertMcusToPixels(struct JpegHeader *jpeg_header,
                         struct JpegMcus *jpeg_mcus, int stride,
                         uint8_t *pixels) {
  uint32_t mcus_x;
  uint32_t mcus_y;
  GetMcusNumbers(jpeg_header, &mcus_x, &mcus_y);

  int16_t *coeffs = jpeg_mcus->coefficients;
  uint8_t *coeffs_num = jpeg_mcus->coefficients_num;
  for (int y = 0; y < mcus_y; ++y) {
    for (int x = 0; x < mcus_x; ++x) {
      // Y component
      for (int yy = 0; yy < 2; ++yy) {
        for (int xx = 0; xx < 2; ++xx) {
          uint8_t *idct_output =
              &pixels[(y * jpeg_header->max_height + yy * 8) * stride +
                      (x * jpeg_header->max_width + xx * 8) * 3];
          InverseDCT8x8(coeffs, *coeffs_num,
                        jpeg_header->per_component[0].dequantization, stride,
                        idct_output);
          coeffs += *coeffs_num;
          coeffs_num++;
        }
      }
      // Skip UV
      coeffs += coeffs_num[0] + coeffs_num[1];
      coeffs_num += 2;
    }
  }
}

void WriteBmpForTest(struct JpegHeader *jpeg_header,
                     struct JpegMcus *jpeg_mcus) {
  uint32_t mcus_x = (jpeg_header->width + jpeg_header->max_width - 1) /
                    jpeg_header->max_width;
  uint32_t mcus_y = (jpeg_header->height + jpeg_header->max_height - 1) /
                    jpeg_header->max_height;

  int stride = mcus_x * jpeg_header->max_width * 3;
  stride = stride + (4 - stride % 4) % 4;
  int size = mcus_y * jpeg_header->max_height * stride;
  uint8_t *pixels = malloc(size);

  ConvertMcusToPixels(jpeg_header, jpeg_mcus, stride, pixels);
  int fd = open("/tmp/asd.bmp", O_WRONLY | O_CREAT | O_TRUNC,
                S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
  static uint8_t header[54] = {66, 77, 0, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0, 40,
                               0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0,  1, 0, 24};
  uint32_t *p = (uint32_t *)&header[2];
  *p = 54 + size;
  p = (uint32_t *)&header[18];
  *p = mcus_x * jpeg_header->max_width;
  p = (uint32_t *)&header[22];
  *p = mcus_y * jpeg_header->max_height;

  write(fd, header, 54);
  for (int y = mcus_y * jpeg_header->max_height - 1; y >= 0; --y) {
    write(fd, &pixels[y * stride], stride);
  }
  close(fd);
  free(pixels);
}

int main(int argc, char *argv[]) {
  if (argc < 2)
    return 1;
  int fd = open(argv[1], O_RDONLY);
  if (fd < 0)
    return 2;
  struct stat s;
  int ret = fstat(fd, &s);
  assert(!ret);
  uint8_t *data = mmap(0, s.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
  assert(data != MAP_FAILED);

  struct timeval tv1, tv2;
  gettimeofday(&tv1, NULL);

  int retry = 1;
  while (retry--) {
  struct JpegHeader jpeg_header = {0};
  uint32_t size = 0;
  ret = ParseJpegHeader(data, s.st_size, &jpeg_header, &size);
  if (!ret) {
    assert(size < s.st_size);

    uint32_t mcus_x =
        (jpeg_header.width + jpeg_header.max_width - 1) / jpeg_header.max_width;
    uint32_t mcus_y = (jpeg_header.height + jpeg_header.max_height - 1) /
                      jpeg_header.max_height;

    struct JpegMcus jpeg_mcus = {0};
    jpeg_mcus.coefficients =
        calloc(1, mcus_x * mcus_y * 64 * 6 * sizeof(int16_t));
    jpeg_mcus.coefficients_num =
        calloc(1, mcus_x * mcus_y * 6 * sizeof(int8_t));

    ret = ScanYUV(data + size, s.st_size - size, &jpeg_header, &jpeg_mcus);
    if (ret) {
      printf("failed to scan %d\n", ret);
    } else {
      WriteBmpForTest(&jpeg_header, &jpeg_mcus);
    }
  gettimeofday(&tv2, NULL);
  printf("Parse Scan stuff = %f ms\n",
         (double)(tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double)(tv2.tv_sec - tv1.tv_sec));

    free(jpeg_mcus.coefficients);
    free(jpeg_mcus.coefficients_num);
  } else {
    printf("failed to parse %d\n", ret);
  }
  }
  munmap(data, s.st_size);
  close(fd);
  return 0;
}
