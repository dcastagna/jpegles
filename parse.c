#include "parse.h"

#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)

#define M_SOF0 0xffc0
#define M_SOF1 0xffc1
#define M_SOF2 0xffc2
#define M_SOF3 0xffc3
#define M_SOF5 0xffc5
#define M_SOF6 0xffc6
#define M_SOF7 0xffc7
#define M_JPG 0xffc8
#define M_SOF9 0xffc9
#define M_SOF10 0xffca
#define M_SOF11 0xffcb
#define M_SOF13 0xffcd
#define M_SOF14 0xffce
#define M_SOF15 0xffcf
#define M_DHT 0xffc4
#define M_DAC 0xffcc
#define M_RST0 0xffd0
#define M_RST1 0xffd1
#define M_RST2 0xffd2
#define M_RST3 0xffd3
#define M_RST4 0xffd4
#define M_RST5 0xffd5
#define M_RST6 0xffd6
#define M_RST7 0xffd7
#define M_SOI 0xffd8
#define H_SOI 0xff
#define L_SOI 0xd8
#define M_EOI 0xffd9
#define H_EOI 0xff
#define L_EOI 0xd9
#define M_SOS 0xffda
#define M_DQT 0xffdb
#define M_DNL 0xffdc
#define M_DRI 0xffdd
#define M_DHP 0xffde
#define M_EXP 0xffdf
#define M_APP0 0xffe0
#define M_APP1 0xffe1
#define M_APP2 0xffe2
#define M_APP3 0xffe3
#define M_APP4 0xffe4
#define M_APP5 0xffe5
#define M_APP6 0xffe6
#define M_APP7 0xffe7
#define M_APP8 0xffe8
#define M_APP9 0xffe9
#define M_APP10 0xffea
#define M_APP11 0xffeb
#define M_APP12 0xffec
#define M_APP13 0xffed
#define M_APP14 0xffee
#define M_APP15 0xffef
#define M_JPG0 0xfff0
#define M_JPG1 0xfff1
#define M_JPG2 0xfff2
#define M_JPG3 0xfff3
#define M_JPG4 0xfff4
#define M_JPG5 0xfff5
#define M_JPG6 0xfff6
#define M_JPG7 0xfff7
#define M_JPG8 0xfff8
#define M_JPG9 0xfff9
#define M_JPG10 0xfffa
#define M_JPG11 0xfffb
#define M_JPG12 0xfffc
#define M_JPG13 0xfffd
#define M_COM 0xfffe
#define M_TEM 0xff01
#define M_RES 0xff02

struct BitReader {
  uint8_t *data;
  uint8_t *data_end;
  uint8_t cache_bits;
  uint64_t cache;
};

// Reads up to 64 bits bit in *out from the reader and returns the actualy number
// of bits read.
int ReadBits(struct BitReader *reader, uint64_t *const out) {
  uint8_t* data = reader->data;
  const uint8_t* data_end = reader->data_end;
  uint8_t cache_bits = reader->cache_bits;
  uint64_t cache = reader->cache;
  while(data != data_end && (cache_bits <= 56)) {
    uint64_t data64 = *data;
    cache |= ( data64 << (56 - cache_bits));
    cache_bits += 8;
    if (*data++ == 0xff && *data++ != 0x00) return -2;
  }
  reader->data = data;
  reader->cache_bits = 0;
  reader->cache = 0;
  *out = cache;
  return cache_bits;
}

int PutBackBits(struct BitReader *reader, const uint8_t num_bits, const uint64_t bits) {
  reader->cache = bits;
  reader->cache_bits = num_bits;
  return 0;
}

int ReadInt16(const uint64_t v64, const uint8_t bits, int16_t * const out) {
  int16_t v = v64 >> (64 - bits);
  if (v < (1 << (bits - 1))) v += (-1 << bits) + 1;
  *out = v;
  return 0;
}

int ReadVlcBigSymbol(const struct VlcTable * const vlc_table, const uint16_t v, uint8_t * const symbol) {
  int code_idx = 0;
  for (int bits = 9; bits <= 16; bits++) {
    int code = v >> (16 - bits);
    while (code_idx < vlc_table->size &&
           vlc_table->map[code_idx].length == bits &&
           vlc_table->map[code_idx].code != code) {
      ++code_idx;
    }

    if (vlc_table->map[code_idx].length == bits &&
        vlc_table->map[code_idx].code == code) {
      *symbol = vlc_table->map[code_idx].value;
      return bits;
    }
  }
  return -6;
}

int ReadVlcSymbol(const struct VlcTable * const vlc_table, const uint16_t v, uint8_t * const symbol) {
  const uint8_t v_small = (v >> 8);
  if ( v_small < vlc_table->small_codes_threshold) {
    const uint8_t length = vlc_table->small_codes[v_small].length;
    if (!length) return -66;
    *symbol = vlc_table->small_codes[v_small].value;
    return length;
  }
  return ReadVlcBigSymbol(vlc_table, v, symbol);
}

static uint8_t kZigzag[64] = {
    0,  1,  8,  16, 9,  2,  3,  10, 17, 24, 32, 25, 18, 11, 4,  5,
    12, 19, 26, 33, 40, 48, 41, 34, 27, 20, 13, 6,  7,  14, 21, 28,
    35, 42, 49, 56, 57, 50, 43, 36, 29, 22, 15, 23, 30, 37, 44, 51,
    58, 59, 52, 45, 38, 31, 39, 46, 53, 60, 61, 54, 47, 55, 62, 63,
};

uint8_t GetZigzag(int index) {
  assert(index < 64);
  return kZigzag[index];
}

int BuildVlcTable(const uint8_t bits[16], const uint8_t *const values,
                  struct VlcTable *vlc_table) {
  uint16_t code = 0;
  const uint8_t* value = values;
  vlc_table->size = 0;
  vlc_table->small_codes_threshold = 0;
  memset(vlc_table->small_codes, 0, sizeof(vlc_table->small_codes));
  // Small codes
  uint8_t b = 1;
  for (; b <= 8; ++b, code <<= 1) {
    uint8_t num = bits[b - 1]; // Number of symbols with b bits
    if (num) {
      if (code + num > (1u << b)) return 19;
      const uint8_t pad = 8 - b;
      for (int s = 0; s < num; ++s, ++code) {
        const uint8_t code_shifted = code << pad;
        const uint8_t max_code = code_shifted + (1 << pad);
        for (uint8_t c=code_shifted; c<max_code; ++c) {
          vlc_table->small_codes[c].length = b;
          vlc_table->small_codes[c].value = *value;
        }
        ++value;
      }
      vlc_table->small_codes_threshold = code << pad;
    }
  }

  for (; b <= 16; ++b, code <<= 1) {
    uint8_t num = bits[b - 1];
    if (num) {
      if (code + num > (1u << b)) return 19;
      for (int s = 0; s < num; ++s, ++code) {
        vlc_table->map[vlc_table->size].code = code;
        vlc_table->map[vlc_table->size].length = b;
        vlc_table->map[vlc_table->size].value = *value;
        ++value;
        vlc_table->size++;
      }
    }
  }
  return 0;
}

int ParseSOF(unsigned char *data, uint32_t size,
             struct JpegHeader *jpeg_header) {
  if (data[0] != 8) // Only 8bpc supported.
    return 8;
  if (size != 8 + MAX_COMPONENTS * data[5]) // Invalid SFO size
    return 9;
  if (data[5] > MAX_COMPONENTS) // At most 3 components supported
    return 10;
  jpeg_header->height = (data[1] << 8) | data[2];
  jpeg_header->width = (data[3] << 8) | data[4];
  jpeg_header->components = data[5];
  for (int c = 0; c < jpeg_header->components; ++c) {
    jpeg_header->per_component[c].index = data[6 + 3 * c] - 1;
    jpeg_header->per_component[c].dimensions = data[7 + 3 * c];
    const int quantization_index = data[8 + 3 * c];
    if (quantization_index >= MAX_COMPONENTS) {
      return 11;
    }
    jpeg_header->per_component[c].dequantization =
        jpeg_header->dequantization[quantization_index];
  }

  return 0;
}

int ParseDHT(unsigned char *data, uint32_t size,
             struct JpegHeader *jpeg_header) {
  uint32_t left = size + 2;
  while (left >= 17) {
    const uint8_t type = data[0] >> 4;    // 0:DC, 1:AC
    const uint8_t index = data[0] & 0x0f; // 0:luma, 1:chroma
    const uint8_t *const bits = data + 1;
    const uint8_t *const values = data + 17;
    if (type >= 2 || index >= 2)
      return 12;
    uint32_t symbols = 0;
    for (int i = 0; i <= 15; ++i) {
      uint8_t s = bits[i];
      if (s > (1 << (i + 1)))
        return 13;
      symbols += s;
    }
    if ((type && symbols > 162) || (!type && symbols > 12))
      return 14;
    if (left < symbols + 17 || symbols > 256)
      return 15;
    uint32_t max_code = 0;
    for (int i = 0; i < symbols; ++i) {
      uint32_t symbol = (uint32_t)values[i];
      if (!type) {
        if (symbol >= 12)
          return 16;
      } else {
        if (!(symbol & 0xf) && symbol != 0 && symbol != 0xf0)
          return 17;
      }
      if (symbol > max_code)
        max_code = symbol;
    }

    if ((!type && max_code > 12) || (type == 1 && max_code > 256))
      return 18;
    // build and validate the coding huffman table
    int ret = BuildVlcTable(bits, values, &jpeg_header->vlc_table[type][index]);
    if (ret)
      return ret;

    data += 17 + symbols;
    left -= 17 + symbols;
  }

  return 0;
}

int ParseDQT(unsigned char *data, uint32_t size,
             struct JpegHeader *jpeg_header) {
  uint32_t left = size + 2;
  while (left >= 65) {
    const uint8_t precision = data[0] >> 4;
    const uint8_t index = data[0] & 0x0f;   // 0:luma, 1:chroma
    const uint8_t *const matrix = data + 1; // 64 bytes
    if (precision || index >= 2)
      return 20;
    for (int i = 0; i < 64; ++i) {
      if (!matrix[i])
        return 21;
      jpeg_header->dequantization[index][i] = matrix[i];
    }
    data += 65;
    left -= 65;
  }

  return 0;
}

int ParseDRI(unsigned char *data, uint32_t size,
             struct JpegHeader *jpeg_header) {
  return 1;
}

int ParseSOS(unsigned char *data, uint32_t size,
             struct JpegHeader *jpeg_header) {
  if (data[0] != jpeg_header->components)
    return 21;
  if (size != 2 * jpeg_header->components + 6)
    return 22;
  for (int i = 0; i < jpeg_header->components; ++i) {
    uint8_t c = jpeg_header->components;
    const uint16_t id = data[1 + 2 * i] - 1;
    while (c-- > 0) {
      if (id == jpeg_header->per_component[c].index)
        break;
    }
    if (c < 0)
      return 23;
    const int w = jpeg_header->per_component[c].dimensions >> 4;
    const int h = jpeg_header->per_component[c].dimensions & 0x0f;
    jpeg_header->per_component[c].blocks = h * w;
    if (h > jpeg_header->max_height)
      jpeg_header->max_height = h;
    if (w > jpeg_header->max_width)
      jpeg_header->max_width = w;

    uint8_t table_idx = data[2 + 2 * i]; // Huffman table index
    if (((table_idx >> 4) >= 2) || ((table_idx & 0x0f) >= 2))
      return 23;
    jpeg_header->table_idx[i] = table_idx;
  }
  jpeg_header->max_height <<= 3;
  jpeg_header->max_width <<= 3;

  return 0;
}

int ParseAPP14(unsigned char *data, uint32_t size,
               struct JpegHeader *jpeg_header) {
  const uint8_t kAdobeHeader[] = {'A', 'd', 'o', 'b', 'e', 0, 100};
  if ((size != 14) || memcmp(data, kAdobeHeader, sizeof(kAdobeHeader)) ||
      data[11] != 1) {
    return 67;
  }
  return 0;
}

int ScanMCU(struct BitReader *bit_reader, const struct JpegHeader *jpeg_header,
            int16_t *dc_coefficients, int16_t *coefficients,
            uint8_t *coefficients_num) {
  uint64_t v64;
  int v64_bits = 0;
  const uint8_t components = jpeg_header->components;
  for (int c = 0; c < components; ++c) {
    const uint8_t table_index = jpeg_header->table_idx[c];
    const struct VlcTable* dc_table = &jpeg_header->vlc_table[0][table_index >> 4];
    const struct VlcTable* ac_table = &jpeg_header->vlc_table[1][table_index & 0xf];
    int blocks = jpeg_header->per_component[c].blocks;
    while (blocks--) {
      if (v64_bits < 16) {
        if (v64_bits) PutBackBits(bit_reader, v64_bits, v64);
        v64_bits = ReadBits(bit_reader, &v64);
        if (unlikely(v64_bits < 1)) return -43;
      }
      // Read the dc component
      uint8_t bits = 0;
      int ret = ReadVlcSymbol(dc_table, v64 >> 48, &bits);
      if (unlikely(ret < 0)) return ret;
      v64_bits -= ret;
      if (unlikely(v64_bits < 0))return -32;
      v64 = v64 << ret;

      int16_t dc = dc_coefficients[c];
      if (bits) {
        if (v64_bits < bits) {
          if (v64_bits) PutBackBits(bit_reader, v64_bits, v64);
          v64_bits = ReadBits(bit_reader, &v64);
          if (unlikely(v64_bits < bits)) return -44;
        }

        ret = ReadInt16(v64, bits, &dc);	
        v64 = v64 << bits;
        v64_bits -= bits;

        dc = dc_coefficients[c] + dc;
      }
      dc_coefficients[c] = dc;
      coefficients[0] = dc;

      // Read the ac components
      int i = 1;
      for (; i < 64; ++i) {
        if (v64_bits < 16) {
          if (v64_bits) PutBackBits(bit_reader, v64_bits, v64);
          v64_bits = ReadBits(bit_reader, &v64);
          if (unlikely(v64_bits < 1)) return -43;
        }
	ret = ReadVlcSymbol(ac_table, v64 >> 48, &bits);
        if (unlikely(ret < 1)) return ret;
        v64_bits -= ret;
        if (unlikely(v64_bits < 0))return -32;
        v64 = v64 << ret;

        if (bits == 0x0) // EOB
          break;
        i += bits >> 4;
        if (bits != 0xf0) { // not escaped
          bits = bits & 0xf;
          if (!bits)
            return 7;
          if (v64_bits < bits) {
            if (v64_bits) PutBackBits(bit_reader, v64_bits, v64);
            v64_bits = ReadBits(bit_reader, &v64);
            if (unlikely(v64_bits < bits)) return -44;
          }
          int16_t ac = v64 >> (64 - bits);
	  if (ac < (1 << (bits - 1))) ac += (-1 << bits) + 1;
          v64 = v64 << bits;
          v64_bits -= bits;
          coefficients[i] = ac;
        }
      }
      *coefficients_num = i;
      ++coefficients_num;
      coefficients += i;
    }
  }

  return PutBackBits(bit_reader, v64_bits, v64);
}

int ScanYUV(unsigned char *data, uint32_t size, struct JpegHeader *jpeg_header,
            struct JpegMcus *jpeg_mcus) {
  if (jpeg_header->components != 3)
    return 1;
  if (jpeg_header->per_component[0].index != 0 ||
      jpeg_header->per_component[0].blocks != 4 ||
      jpeg_header->per_component[0].dimensions != 0x22 ||
      jpeg_header->per_component[1].index != 1 ||
      jpeg_header->per_component[1].blocks != 1 ||
      jpeg_header->per_component[1].dimensions != 0x11 ||
      jpeg_header->per_component[2].index != 2 ||
      jpeg_header->per_component[2].blocks != 1 ||
      jpeg_header->per_component[2].dimensions != 0x11) {
    return 2;
  }

  uint32_t mcus_x;
  uint32_t mcus_y;
  GetMcusNumbers(jpeg_header, &mcus_x, &mcus_y);

  int16_t dc_coefficients[MAX_COMPONENTS] = {0, 0, 0};
  struct BitReader bit_reader = {data, data + size - 2, 0, 0};
  int16_t *coefficients = jpeg_mcus->coefficients;
  uint8_t *coefficients_num = jpeg_mcus->coefficients_num;
  for (int y = 0; y < mcus_y; ++y) {
    for (int x = 0; x < mcus_x; ++x) {
      int ret = ScanMCU(&bit_reader, jpeg_header, dc_coefficients, coefficients,
                        coefficients_num);
      if (ret)
        return ret;
      for (int i = 0; i < 6; ++i)
        coefficients += coefficients_num[i];
      coefficients_num += 6; 
    }
  }
  return 0;
}

int ParseJpegHeader(unsigned char *data, uint32_t size,
                    struct JpegHeader *jpeg_header, uint32_t *out_position) {
  if (size < 2 || data[0] != H_SOI || data[1] != L_SOI)
    return 1; // Invalid JPEG.
  if (size < 6 || data[size - 2] != H_EOI || data[size - 1] != L_EOI)
    return 2; // JPEG input seems truncated.

  uint32_t i = 2; // We skip the first two bytes we just checked.
  uint32_t marker;
  uint32_t marker_seen = 0;
  do {
    if (i + 4 > size)
      return 3; // Truncate header.

    if (data[i] != 0xff)
      return 4; // Not at header boundaries.

    marker = (data[i] << 8) | data[i + 1];
    i += 2;
    uint32_t marker_size = ((data[i] << 8) | data[i + 1]);
    i += 2;
    if (i + marker_size > size)
      return 5; // Invalid marker size.

    struct {
      uint32_t m;
      int (*f)(unsigned char *, uint32_t, struct JpegHeader *);
    } funcs[] = {{M_SOF0, ParseSOF}, {M_DHT, ParseDHT}, {M_DQT, ParseDQT},
                 {M_DRI, ParseDRI},  {M_SOS, ParseSOS}, {M_APP14, ParseAPP14}};

    for (int f = 0; f < sizeof(funcs) / sizeof(funcs[0]); ++f) {
      if (funcs[f].m == marker) {
        marker_seen |= 1 << f;
        int ret = funcs[f].f(data + i, marker_size, jpeg_header);
        if (ret)
          return ret;
        break;
      }
    }
    i += marker_size - 2;
  } while (marker != M_SOS);

  if (marker_seen != 0x17 && // SOF0|DHT|DQT|SOS
      marker_seen != 0x37)   // SOF0|DHT|DQT|SOS|APP14
    return 6;
  *out_position = i;
  return 0;
}

void GetMcusNumbers(struct JpegHeader *jpeg_header, uint32_t *mcus_x,
                    uint32_t *mcus_y) {
  assert(mcus_x && mcus_y);
  *mcus_x = (jpeg_header->width + jpeg_header->max_width - 1) /
            jpeg_header->max_width;
  *mcus_y = (jpeg_header->height + jpeg_header->max_height - 1) /
            jpeg_header->max_height;
}
