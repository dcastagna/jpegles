#include <stdint.h>

#define MAX_COMPONENTS 3

struct VlcTable {
  struct {
    uint8_t length;
    uint8_t value;
  } small_codes[1 << 8];  // Codes
  uint16_t small_codes_threshold;

  struct {
    uint16_t code;
    uint8_t length;
    uint8_t value;
  } map[256];
  uint16_t size;
};

struct JpegHeader {
  uint32_t width;
  uint32_t height;
  uint32_t max_width;
  uint32_t max_height;
  uint8_t components;
  struct {
    uint8_t index;
    uint8_t dimensions;
    uint8_t *dequantization; // Pointer to dequantization[]
    uint32_t blocks;
  } per_component[MAX_COMPONENTS];
  uint8_t dequantization[MAX_COMPONENTS][64]; // Coeff of dequant
  uint8_t table_idx[MAX_COMPONENTS]; // Huffman table index per component
  struct VlcTable vlc_table[2][2];
};

struct JpegMcus {
  int16_t *coefficients;
  uint8_t *coefficients_num;
};

int ParseJpegHeader(unsigned char *data, uint32_t size,
                    struct JpegHeader *jpeg_header, uint32_t *out_position);

int ScanYUV(unsigned char *data, uint32_t size, struct JpegHeader *jpeg_header,
            struct JpegMcus *jpeg_mcus);

uint8_t GetZigzag(int index);

void GetMcusNumbers(struct JpegHeader *jpeg_header, uint32_t *mcus_x,
                    uint32_t *mcus_y);
