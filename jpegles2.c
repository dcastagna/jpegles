#include "parse.h"

#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#define GL_RED_INTEGER 0x8D94
#define GL_R16I 0x8233

void glVertexAttribDivisor(GLuint index, GLuint divisor);
void glDrawArraysInstanced(GLenum mode, GLint first, GLsizei count,
                           GLsizei primcount);
void glUniform2ui(GLint location, GLuint v0, GLuint v1);

GLuint LoadShader(const GLenum type, const char *const src) {
  GLuint shader = 0;
  shader = glCreateShader(type);
  assert(shader);
  glShaderSource(shader, 1, &src, NULL);
  glCompileShader(shader);

  GLint compiled = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
  if (!compiled) {
    GLint len = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
    if (len > 1) {
      char *error_log = malloc(len);
      glGetShaderInfoLog(shader, len, NULL, error_log);
      printf("Error compiling shader: %s", error_log);
      free(error_log);
    }
  }
  assert(compiled);
  return shader;
}

#define SHADER(Src) #Src
// clang-format off
const char kVertexShader[] =
"#version 300 es\n"
"precision mediump float;\n"
"precision mediump isampler2D;\n"
SHADER(
  in vec2 a_position;
  in float a_instance_id;
  in float a_idct_id;
  in float a_idct_num;
  uniform isampler2D a_idct;
  uniform ivec2 size;
  uniform ivec2 idct_size;
  uniform int dequant[64];

  flat out mat4 idcts[4];
  out vec2 mn;
  void main() {
    int instance = int(round(a_instance_id));
    int instance_x = instance % ( (size.x + 1) / 2);
    int instance_y = instance / ( (size.x + 1) / 2);
    vec2 translation = vec2(float(instance_x * 2) / float(size.x), float(instance_y * 8) / float(size.y));
    vec2 position = vec2(translation.x + a_position.x, translation.y + a_position.y) * 2.0 - vec2(1.0, 1.0);
    gl_Position = vec4(position.x, position.y , 0.0, 1.0);
    // TODO: Figure out how interpolation works here.
    mn = vec2(-2.0, -0.5);
    if (a_position.x > 0.0) mn.x = 6.0;
    if (a_position.y > 0.0) mn.y = 7.5;

    for (int i=0; i<4; i++)
      idcts[i] = mat4(0.0);
    for (int i=0; i<int(a_idct_num); i++) {
      int idct = int(a_idct_id) + i;
      int idct_u = idct % idct_size.x;
      int idct_v = idct / idct_size.x;
      vec2 idct_uv = vec2(float(idct_u)/float(idct_size.x) + 0.5 / float(idct_size.x),
			  float(idct_v)/float(idct_size.y) + 0.5 / float(idct_size.y));

      int idct_mat = i / 16;
      int idct_mod = i % 16;
      int c = texture(a_idct, idct_uv).x;
      idcts[idct_mat][idct_mod / 4][ idct_mod % 4] = float(c * dequant[i]);
    }
  }
);

const char kFragmentShader[] =
"#version 300 es\n"
"precision mediump float;\n"
SHADER(
  flat in mat4 idcts[4];
  in vec2 mn;
  out vec4 fragmentColor;
  void main() {
    int zigzag[64] = int[](
    0,  1,  8,  16, 9,  2,  3,  10, 17, 24, 32, 25, 18, 11, 4,  5,
    12, 19, 26, 33, 40, 48, 41, 34, 27, 20, 13, 6,  7,  14, 21, 28,
    35, 42, 49, 56, 57, 50, 43, 36, 29, 22, 15, 23, 30, 37, 44, 51,
    58, 59, 52, 45, 38, 31, 39, 46, 53, 60, 61, 54, 47, 55, 62, 63);

    float c0 = 1.0 / sqrt(8.0);
    float cr = 0.5;
    float M_PI = 3.1415926535897932384626433832795;
    // values are cos((M_PI * (2.0 * mz + 1.0) * float(pz)) / 16.0)
    // with mz = index / 8 and pz = index % 8;
    float cosines[64] = float[](1.000000000000000000000000000000000,0.980785280403230430579242238309234,0.923879532511286738483136105060112,0.831469612302545235671402679145103,0.707106781186547572737310929369414,0.555570233019602288671023870847421,0.382683432365089837290383911749814,0.195090322016128331350515168196580,1.000000000000000000000000000000000,0.831469612302545235671402679145103,0.382683432365089837290383911749814,-0.195090322016128192572637090052012,-0.707106781186547461715008466853760,-0.980785280403230430579242238309234,-0.923879532511286849505438567575766,-0.555570233019602177648721408331767,1.000000000000000000000000000000000,0.555570233019602288671023870847421,-0.382683432365089726268081449234160,-0.980785280403230430579242238309234,-0.707106781186547683759613391885068,0.195090322016128303594939552567666,0.923879532511286516438531180028804,0.831469612302545457716007604176411,1.000000000000000000000000000000000,0.195090322016128331350515168196580,-0.923879532511286738483136105060112,-0.555570233019602177648721408331767,0.707106781186547350692706004338106,0.831469612302545457716007604176411,-0.382683432365089892801535143007641,-0.980785280403230652623847163340542,1.000000000000000000000000000000000,-0.195090322016128192572637090052012,-0.923879532511286849505438567575766,0.555570233019601844581814020784805,0.707106781186547683759613391885068,-0.831469612302545124649100216629449,-0.382683432365090558935349918101565,0.980785280403230430579242238309234,1.000000000000000000000000000000000,-0.555570233019601955604116483300459,-0.382683432365090336890744993070257,0.980785280403230652623847163340542,-0.707106781186546684558891229244182,-0.195090322016129774640447180900082,0.923879532511287404616950880154036,-0.831469612302544014426075591472909,1.000000000000000000000000000000000,-0.831469612302545346693705141660757,0.382683432365090003823837605523295,0.195090322016127915016880933762877,-0.707106781186547128648101079306798,0.980785280403230319556939775793580,-0.923879532511287071550043492607074,0.555570233019604398094770658644848,1.000000000000000000000000000000000,-0.980785280403230430579242238309234,0.923879532511286516438531180028804,-0.831469612302544236470680516504217,0.707106781186546573536588766728528,-0.555570233019601511514906633237842,0.382683432365086284576705111248884,-0.195090322016125083948168139613699);
    for (int p=0; p<4; p++) {
      float mz = mn.y;
      float nz = mn.x + float(p);
      float c = 0.0;
      for (int i=0; i<64; i++) {
	int pz = zigzag[i] / 8;
	int qz = zigzag[i] % 8;
	int idct_mat = i / 16;
	int idct_mod = i % 16;
	float idct_coeff = idcts[idct_mat][idct_mod / 4][ idct_mod % 4];
	c += (pz > 0 ? cr : c0) * (qz > 0 ? cr : c0) * idct_coeff *
	  cosines[int(round(mz)) * 8 + pz] *
	  cosines[int(round(nz)) * 8 + qz];
      }
      c += 128.0;
      c /= 256.0;
      c = clamp(c, 0.0, 1.0);
      fragmentColor[p] = c;
    }
  }
);
// clang-format off
void LoadProgram(GLuint *program, GLuint *vertex, GLuint *fragment) {
  *vertex = LoadShader(GL_VERTEX_SHADER, kVertexShader);
  *fragment = LoadShader(GL_FRAGMENT_SHADER, kFragmentShader);
  *program = glCreateProgram();

  assert(*program);
}

GLuint GenAndBindTexture() {
  assert(glGetError() == GL_NO_ERROR);
  GLuint texture;
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  assert(glGetError() == GL_NO_ERROR);
  return texture;
}

int Convert(struct JpegHeader *jpeg_header, struct JpegMcus *jpeg_mcus) {
  // Setup the destination texture
  GLuint color_texture = GenAndBindTexture();
  int texture_width = (jpeg_header->width + 3) / 4;
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture_width,
               jpeg_header->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
  assert(glGetError() == GL_NO_ERROR);
  GLuint framebuffer_object;
  glGenFramebuffers(1, &framebuffer_object);
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_object);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                         color_texture, 0);
  assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

  // Load program and shaders
  GLuint program, vertex, fragment;
  LoadProgram(&program, &vertex, &fragment);
  glAttachShader(program, vertex);
  glAttachShader(program, fragment);
  glLinkProgram(program);
  GLint linked = -1;
  glGetProgramiv(program, GL_LINK_STATUS, &linked);
  if (!linked) {
    GLint len = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
    if (len > 1) {
      char *error_log = malloc(len);
      glGetProgramInfoLog(program, len, NULL, error_log);
      printf("Error linking program: %s", error_log);
      free(error_log);
    }
  }
  assert(0 != linked);
  struct timeval tv1, tv2;
  gettimeofday(&tv1, NULL);
  glUseProgram(program);
  glUniform2i(glGetUniformLocation(program, "size"), texture_width, jpeg_header->height);
  // Setup vertex buffers
  GLuint vertex_buffers[4] = {0};
  glGenBuffers(4, vertex_buffers);
  assert(vertex_buffers[0] && vertex_buffers[1] && vertex_buffers[2] && vertex_buffers[3]);

  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffers[0]);
  // Position
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 2, 0);
  glEnableVertexAttribArray(0);
  GLfloat right = 2.0f / texture_width;  // 4 channel per pixel
  GLfloat top = 8.0f / jpeg_header->height;

  // clang-format off
  GLfloat data[16] = {
    0.f, 0.f,
    right, 0.f,
    0.f, top,
    right, top};
  // clang-format on
  glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW);
  assert(glGetError() == GL_NO_ERROR);

  uint32_t mcus_x;
  uint32_t mcus_y;
  GetMcusNumbers(jpeg_header, &mcus_x, &mcus_y);
  uint16_t *instances = malloc(sizeof(uint16_t) * mcus_x * 2 * mcus_y * 2);
  uint32_t *instances_offsets =
      malloc(sizeof(uint32_t) * mcus_x * 2 * mcus_y * 2);
  uint8_t *instances_coeffs = malloc(sizeof(uint8_t) * mcus_x * 2 * mcus_y * 2);

  uint16_t instances_size = 0;

  int dct_coeffs_index = 0;
  uint8_t *coeffs_num = jpeg_mcus->coefficients_num;
  int block_stride = (texture_width + 1) / 2;
  for (int y = 0; y < mcus_y; ++y) {  // only top left
    for (int x = 0; x < mcus_x; ++x) {
      for (int m_y = 0; m_y < 2; ++m_y) {
        for (int m_x = 0; m_x < 2; ++m_x) {
          int block_x = x * 2 + m_x;
          int block_y = y * 2 + m_y;
          if (//*coeffs_num <=16 &&
              block_x < block_stride &&
              block_y < ((jpeg_header->height + 7) / 8)) {
            if ((y * 2 + m_y) * block_stride + x * 2 + m_x > (1 << 16)) {
              printf("Too many MCUs, can't fit instance index in an uint16.\n");
              goto outloop;

            }
            instances[instances_size] =
                (y * 2 + m_y) * block_stride + x * 2 + m_x;
            instances_offsets[instances_size] = dct_coeffs_index;
            instances_coeffs[instances_size] = *coeffs_num;
            instances_size++;
          }
          dct_coeffs_index += *coeffs_num;
          coeffs_num++;
        }
      }
      dct_coeffs_index += coeffs_num[0] + coeffs_num[1];
      coeffs_num += 2;  // Skip UV
    }
  }
outloop:

  //
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffers[1]);
  glVertexAttribPointer(1, 1, GL_UNSIGNED_SHORT, GL_FALSE, sizeof(uint16_t), 0);
  glVertexAttribDivisor(1, 1);
  glEnableVertexAttribArray(1);
  glBufferData(GL_ARRAY_BUFFER, sizeof(uint16_t) * instances_size, instances,
               GL_STATIC_DRAW);
  assert(glGetError() == GL_NO_ERROR);
  free(instances);

  // idct offsets
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffers[2]);
  glVertexAttribPointer(2, 1, GL_UNSIGNED_INT, GL_FALSE, sizeof(uint32_t), 0);
  glVertexAttribDivisor(2, 1);
  glEnableVertexAttribArray(2);
  glBufferData(GL_ARRAY_BUFFER, sizeof(uint32_t) * instances_size,
               instances_offsets, GL_STATIC_DRAW);
  assert(glGetError() == GL_NO_ERROR);
  free(instances_offsets);

  // idct coeffs num
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffers[3]);
  glVertexAttribPointer(3, 1, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(uint8_t), 0);
  glVertexAttribDivisor(3, 1);
  glEnableVertexAttribArray(3);
  glBufferData(GL_ARRAY_BUFFER, sizeof(uint8_t) * instances_size,
               instances_coeffs, GL_STATIC_DRAW);
  assert(glGetError() == GL_NO_ERROR);
  free(instances_coeffs);

  // Set dequant
  GLint dequant[64];
  for (int i = 0; i < 64; ++i) {
    dequant[i] = jpeg_header->per_component[0].dequantization[i];
  }
  glUniform1iv(glGetUniformLocation(program, "dequant"), 64, dequant);
  assert(glGetError() == GL_NO_ERROR);

  // Upload jpeg data
  int idct_size = 0;
  for (int i = 0; i < mcus_x * mcus_y * 6; ++i) {
    idct_size += jpeg_mcus->coefficients_num[i];
  }
  int width = 1024;
  int height = (idct_size + width - 1) / width;
  assert(width * height <= mcus_x * mcus_y * 6 * 64);

  glActiveTexture(GL_TEXTURE0);
  GLuint idct_texture = GenAndBindTexture();
  glTexImage2D(GL_TEXTURE_2D, 0, GL_R16I, width, height, 0, GL_RED_INTEGER,
               GL_SHORT, jpeg_mcus->coefficients);
  glUniform1i(glGetUniformLocation(program, "a_idct"), 0);  // Coeff texture
  assert(glGetError() == GL_NO_ERROR);
  glUniform2i(glGetUniformLocation(program, "idct_size"), width, height);
  assert(glGetError() == GL_NO_ERROR);

  glViewport(0, 0, texture_width, jpeg_header->height);
  assert(glGetError() == GL_NO_ERROR);

  glClearColor(0.3, 0.3, 0.3, 0.3);
  glClear(GL_COLOR_BUFFER_BIT);
  glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, instances_size);
  glFinish();
  gettimeofday(&tv2, NULL);
  printf("GL Stuff size = %f seconds\n",
         (double)(tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double)(tv2.tv_sec - tv1.tv_sec));

  GLubyte *pixels =
        malloc(sizeof(GLubyte) * 4 * texture_width * jpeg_header->height);

    glReadPixels(0, 0, texture_width, jpeg_header->height, GL_RGBA,
                 GL_UNSIGNED_BYTE, pixels);
  assert(glGetError() == GL_NO_ERROR);

  glDeleteTextures(1, &idct_texture);
  glDeleteTextures(1, &color_texture);
  glDeleteFramebuffers(1, &framebuffer_object);

  glDeleteBuffers(4, vertex_buffers);
  glDeleteProgram(program);
  glDeleteShader(vertex);
  glDeleteShader(fragment);
  printf("Done GL stuff\n");

  int stride = (texture_width * 4) * 3;
  int size = stride * jpeg_header->height;
  int fd = open("/tmp/asd.bmp", O_WRONLY | O_CREAT | O_TRUNC,
                S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
  static uint8_t header[54] = {66, 77, 0, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0, 40,
                               0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0,  1, 0, 24};
    uint32_t *p = (uint32_t *)&header[2];
  *p = 54 + size;
  p = (uint32_t *)&header[18];
  *p = texture_width * 4;
  p = (uint32_t *)&header[22];
  *p = jpeg_header->height;

  write(fd, header, 54);
  uint8_t* row = malloc(texture_width * 4 * 3);
  for (int y = jpeg_header->height - 1; y >= 0; --y) {
    int x = 0;
    for (; x < texture_width * 4; ++x) {
      memset(&row[x * 3], pixels[y * texture_width * 4 + x], 3);
    }
    write(fd, row, x * 3);
    if (stride - x * 3) {
      write(fd, pixels, stride - x * 3);
    }
  }
  free(row);
  close(fd);
   free(pixels);

  return 0;
}

int ParseAndConvertJpeg(char *filename) {
  int fd = open(filename, O_RDONLY);
  if (fd < 0) return 2;
  struct stat s;
  int ret = fstat(fd, &s);
  assert(!ret);
  uint8_t *data = mmap(0, s.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
  assert(data != MAP_FAILED);

  // Parse jpeg
  struct timeval tv1, tv2;
  gettimeofday(&tv1, NULL);

  struct JpegHeader jpeg_header = {0};
  uint32_t size = 0;
  ret = ParseJpegHeader(data, s.st_size, &jpeg_header, &size);
    gettimeofday(&tv2, NULL);
  printf("Parsed jpeg header = %f seconds\n",
           (double)(tv2.tv_usec - tv1.tv_usec) / 1000000 +
               (double)(tv2.tv_sec - tv1.tv_sec));

  if (!ret) {
    uint32_t retry = 1;
    while (retry--) {
    assert(size < s.st_size);
    uint32_t mcus_x;
    uint32_t mcus_y;
    GetMcusNumbers(&jpeg_header, &mcus_x, &mcus_y);

    struct JpegMcus jpeg_mcus = {0};
    jpeg_mcus.coefficients =
        calloc(1, mcus_x * mcus_y * 64 * 6 * sizeof(int16_t));
    jpeg_mcus.coefficients_num =
        calloc(1, mcus_x * mcus_y * 6 * sizeof(int8_t));
    ret = ScanYUV(data + size, s.st_size - size, &jpeg_header, &jpeg_mcus);
    gettimeofday(&tv2, NULL);
    printf("Parsed jpeg = %f seconds\n",
           (double)(tv2.tv_usec - tv1.tv_usec) / 1000000 +
               (double)(tv2.tv_sec - tv1.tv_sec));

    if (ret) {
      printf("failed to scan %d\n", ret);
    } else {
      Convert(&jpeg_header, &jpeg_mcus);
    }

    free(jpeg_mcus.coefficients);
    free(jpeg_mcus.coefficients_num);
    }
  }
  munmap(data, s.st_size);
  close(fd);
  return ret;
}

int main(int argc, char *argv[]) {
  if (argc < 2)  // Filename to read
    return 1;

  // Setup EGL
  EGLDisplay egl_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
  assert(egl_display != EGL_NO_DISPLAY);
  EGLint major, minor;
  EGLBoolean ret = eglInitialize(egl_display, &major, &minor);
  assert(ret);

  ret = eglBindAPI(EGL_OPENGL_ES_API);
  assert(ret);
  EGLint attr[] = {// some attributes to set up our egl-interface
                   EGL_BUFFER_SIZE,
                   32,
                   EGL_RED_SIZE,
                   8,
                   EGL_GREEN_SIZE,
                   8,
                   EGL_BLUE_SIZE,
                   8,
                   EGL_ALPHA_SIZE,
                   8,
                   EGL_RENDERABLE_TYPE,
                   EGL_OPENGL_ES2_BIT,
                   EGL_SURFACE_TYPE,
                   EGL_DONT_CARE,
                   EGL_NONE};
  EGLConfig config;
  EGLint num_config;
  EGLint r = eglChooseConfig(egl_display, attr, &config, 1, &num_config);
  assert(r != EGL_FALSE);
  assert(num_config == 1);

  // GLES3.0
  EGLint context_attribs[] = {EGL_CONTEXT_CLIENT_VERSION, 3, EGL_NONE,
                              EGL_NONE};
  // TODO: check for EGL_KHR_surfaceless_context
  EGLContext egl_context =
      eglCreateContext(egl_display, config, 0, context_attribs);
  assert(egl_context != EGL_NO_CONTEXT);
  ret =
      eglMakeCurrent(egl_display, EGL_NO_SURFACE, EGL_NO_SURFACE, egl_context);

  assert(ret);

  printf("%s\n", glGetString(GL_VERSION));

  ParseAndConvertJpeg(argv[1]);

  eglDestroyContext(egl_display, egl_context);
  eglTerminate(egl_display);

  return 0;
}
